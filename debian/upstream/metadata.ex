# Bug-Database: https://gitlab.com/dart-os/packages/dartos-plymouth-theme/issues
# Bug-Submit: https://gitlab.com/dart-os/packages/dartos-plymouth-theme/issues/new
# Changelog: https://gitlab.com/dart-os/packages/dartos-plymouth-theme/blob/master/CHANGES
# Documentation: https://gitlab.com/dart-os/packages/dartos-plymouth-theme/wiki
# Repository-Browse: https://gitlab.com/dart-os/packages/dartos-plymouth-theme
# Repository: https://gitlab.com/dart-os/packages/dartos-plymouth-theme.git
