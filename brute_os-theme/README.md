##<font color="#55aa00"><b>brute_os-plymouth-theme</b></font>

* __`Version:`__ 0.1~alpha-1
* __`Date:`__ 27.03.2023
* __`License:`__  [GPL-3.0](https://www.gnu.org/licenses/gpl-3.0.txt)
* __`Copyright:`__ Copyright (C) 2023 Arijit Bhowmick <arijit_bhowmick@outlook.com>
* __`Author:`__ [Arijit Bhowmick](https://sys41x4.github.io/)

---------------------------------

## Package:
brute_os-plymouth-theme.deb


---------------------------------
### Changelog:
>
___`v0.1~alpha (19.06.2023)`___
>
* `First alpha release`
